package ru.t1.karimov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
