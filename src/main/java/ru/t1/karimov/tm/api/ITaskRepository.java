package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

}
