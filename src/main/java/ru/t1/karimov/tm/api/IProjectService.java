package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description);

}
