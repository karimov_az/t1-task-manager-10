package ru.t1.karimov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
