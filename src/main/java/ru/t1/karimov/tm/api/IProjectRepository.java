package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

}
