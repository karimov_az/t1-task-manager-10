package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
