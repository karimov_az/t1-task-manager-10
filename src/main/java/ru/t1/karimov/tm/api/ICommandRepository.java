package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
